package com.example.liurenju.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity{

    private TextView mTextView;
    TextView screen;
    private String display = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        screen = (TextView)findViewById(R.id.textView);
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTextView = (TextView) stub.findViewById(R.id.text);
            }
        });
    }

    public void button1(View view){
        screen = (TextView)findViewById(R.id.textView);
        display += "1";
        screen.setText(display);
    }

    public void button2(View view){
        screen = (TextView)findViewById(R.id.textView);
        display += "2";
        screen.setText(display);
    }

    public void button3(View view){
        screen = (TextView)findViewById(R.id.textView);
        display += "3";
        screen.setText(display);
    }

    public void button4(View view){
        screen = (TextView)findViewById(R.id.textView);
        display += "4";
        screen.setText(display);
    }

    public void button5(View view){
        screen = (TextView)findViewById(R.id.textView);
        display += "5";
        screen.setText(display);
    }

    public void button6(View view){
        screen = (TextView)findViewById(R.id.textView);
        display += "6";
        screen.setText(display);
    }

    public void button7(View view){
        screen = (TextView)findViewById(R.id.textView);
        display += "7";
        screen.setText(display);
    }

    public void button8(View view){
        screen = (TextView)findViewById(R.id.textView);
        display += "8";
        screen.setText(display);
    }

    public void plusButton(View view){
        screen = (TextView)findViewById(R.id.textView);
        display += "+";
        screen.setText(display);
    }

    public void equalsButton(View view) throws Exception{
        screen = (TextView)findViewById(R.id.textView);
        String results[] = display.split("\\+");
        if (results.length == 0){
            screen.setText(display);
            return;
        }
        else if(display.substring(display.length()-1).equals("+")){
            display = "Error";
            screen.setText(display);
            display = "";
            return;
        }
        int result = 0;
        try {
            for (int i = 0; i < results.length; i++) {
                result += Integer.parseInt(results[i]);
            }
            display = "" + result;
            screen.setText(display);
        }
        catch (Exception e){
            display = "Error";
            screen.setText(display);
            display = "";
        }
}

    public void clearButton(View view){
        screen = (TextView)findViewById(R.id.textView);
        display = "";
        screen.setText(display);
    }
}
